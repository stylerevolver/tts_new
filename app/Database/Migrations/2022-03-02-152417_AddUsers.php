<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUsers extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'pass' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'phoneno' => [
                'type' => 'VARCHAR',
                'constraint' => '12',
                'null' => true
            ],
            'gender' => [
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => true
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'city_id' => [
                'type' => 'INT',
                'unsigned' => TRUE,
                'null' => true
            ],
            'dob' => [
                'type' => 'date',
                'null' => true
            ],
            'role' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'profilepic' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'status' => [
                'type' => 'INT',
                'constraint' => '5',
                'null' => true
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
        'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('city_id', 'city', 'id','CASCADE','CASCADE');
        $this->forge->createTable('users');
    }

    public function down()
    {
        $this->forge->dropTable('users');
    }
}
