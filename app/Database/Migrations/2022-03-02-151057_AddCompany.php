<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddCompany extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true
            ],
            'phoneno' => [
                'type' => 'VARCHAR',
                'constraint' => '12',
                'null' => true
            ],
            'city_id' => [
                'type' => 'INT',
                'unsigned' => TRUE,
                'null' => false
            ],
            'status' => [
                'type' => 'INT',
                'constraint' => '5',
                'null' => true
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
        'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('city_id', 'city', 'id','CASCADE','CASCADE');
        $this->forge->createTable('company');
    }

    public function down()
    {
        $this->forge->dropTable('company');
    }
}
