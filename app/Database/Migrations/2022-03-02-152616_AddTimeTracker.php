<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTimeTracker extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'description' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => false
            ],
            'company_id' => [
                'type' => 'INT',
                'unsigned' => TRUE
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => TRUE
            ],
            'project_id' => [
                'type' => 'INT',
                'unsigned' => TRUE
            ],
            'startdate' => [
                'type' => 'date',
                'null' => true,
            ],
            'enddate' => [
                'type' => 'date',
                'null' => true,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '25',
                'default'=>'created'
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
        'created_at datetime default current_timestamp',
        ]);
        $this->forge->addForeignKey('company_id', 'company', 'id','CASCADE','CASCADE');
        $this->forge->addForeignKey('project_id', 'project', 'id','CASCADE','CASCADE');
        $this->forge->addForeignKey('user_id', 'users', 'id','CASCADE','CASCADE');
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('time_tracker');
    }

    public function down()
    {
        $this->forge->dropTable('time_tracker');
    }
}
