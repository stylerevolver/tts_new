<?php

namespace App\Database\Seeds;
use App\Models\UserModel;

use CodeIgniter\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $user_object = new UserModel();

        $user_object->insertBatch([
            [
                "name" => "Krisha Patel",
                "email" => "krishna@gmail.com",
                "phoneno" => "7899654125",
                "dob" => "2000/07/29",
                "role" => "admin",
                "status" => 1,
                "pass" => password_hash("12345678", PASSWORD_DEFAULT),
                "address" => "Zadeshwar bharuch",
                "city_id" =>215,
                "gender" =>"female",
            ],
            [
                "name" => "Hardik prajapati",
                "email" => "hardik@gmail.com",
                "phoneno" => "8888888888",
                "role" => "employee",
                "dob" => "1999/01/31",
                "status" => 1,
                "pass" => password_hash("12345678", PASSWORD_DEFAULT),
                "address" => "381 Mahadev vas-1, new vadaj",
                "city_id" =>210,
                "gender" =>"male",
            ]
        ]);
    }
}
