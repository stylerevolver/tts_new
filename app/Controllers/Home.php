<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        if (!session()->get('isLoggedIn')) {
            return redirect()->to(site_url('login'));
        }else{
            if (session()->get('role') == "employee"){
                return redirect()->to(site_url('employee'));
            }else{
                return redirect()->to(site_url('admin'));
            }
        }
    }
}
