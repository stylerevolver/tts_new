<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CompanyModel;
use App\Models\ProjectModel;
use App\Models\UserModel;
use App\Models\TimeTrackerModel;


class ReportController extends BaseController
{

    public $session;
    public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
    }

    public function index()
    {
        $data=[];
        $companyModel = new CompanyModel();
        $timetracker=new TimeTrackerModel();
        $projectModel=new ProjectModel();
        $userModel=new UserModel();
        
        $data['tickets']=$timetracker->findAll();
        $data['projects']=$projectModel->findAll();
        $data['users']=$userModel->findAll();

        return view('admin/reports/index',$data);
    }

    public function create(){
        date_default_timezone_set('Asia/Kolkata');
        $data['now'] = date("d-m-Y");
        
        $errors = [];
        
        $ticketModel = new TimeTrackerModel();
        
        $projectModel = new ProjectModel();
        
        
        
        // echo $user=$this->request->getVar('user'); echo "<br>";
        // echo $project=$this->request->getVar('project'); echo "<br>";
        // echo $sdate=$this->request->getVar('sdate'); echo "<br>";
        // echo $edate=$this->request->getVar('edate');  echo "<br>";
        // exit;

        $user=$this->request->getVar('user');
        $project=$this->request->getVar('project');
        $sdate=$this->request->getVar('sdate');
        $edate=$this->request->getVar('edate');
        $title="TIME MANAGEMENT REPORT";
        //userwise
        if ($user != "" && $project == "" && $sdate == "" && $edate == ""){
            $title="User Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.user_id=$user;";
        }
        //project wise
        else if ($user == "" && $project != "" && $sdate == "" && $edate == ""){
            $title="Project Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.project_id=$project;";
        }

        //start date
        else if ($user == "" && $project == "" && $sdate != "" && $edate == ""){
            $title="Start Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.startdate >='$sdate';";
        }
        //end date
        else if ($user == "" && $project == "" && $sdate == "" && $edate != ""){
            $title="End Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.enddate >='$edate';";
        }
        //user and project
        else if ($user != "" && $project != "" && $sdate == "" && $edate == ""){
            $title="User And Project Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.user_id=$user and t.project_id=$project;";
        }
        //user and sdate
        else if ($user != "" && $project == "" && $sdate != "" && $edate == ""){
            $title="User And Start Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.user_id=$user and t.startdate >='$sdate';";
        }
        //user and edate
        else if ($user != "" && $project == "" && $sdate == "" && $edate != ""){
            $title="User And End Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.user_id=$user and t.enddate >='$edate';";
        }
        //project and sdate
        else if ($user == "" && $project != "" && $sdate != "" && $edate == ""){
            $title="Project And Start Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.project_id=$project and t.startdate >='$sdate';";
        }
        //project and edate
        else if ($user = "" && $project != "" && $sdate == "" && $edate != ""){
            $title="Project And End Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.project_id=$project and t.enddate >='$edate';";
        }
        //sdate and edate
        else if ($user == "" && $project == "" && $sdate != "" && $edate != ""){
            $title="Start Date And End Date Wise TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.startdate >='$sdate' and t.enddate <='$edate';";
        }
        //all
        else if ($user != "" && $project != "" && $sdate != "" && $edate != ""){
            $title="TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id and t.startdate >='$sdate' and t.enddate <='$edate';";
        }else{
            $title="TIME MANAGEMENT REPORT";
            $tsql="SELECT ut.id, ut.status, ut.time, ut.remarks, ut.date, ut.status,p.name as project_name, u.name AS user_name FROM `time_tracker` AS t, user_time_tracker ut, users u,project as p WHERE t.id = ut.time_tracker_id AND u.id = t.user_id and p.id=t.project_id";
        }

       // echo $tsql;exit();
        $data['title']=$title;
        $tquery = $this->db->query($tsql);
        $data['tickets_details'] = $tquery->getResult();
        if($this->request->getVar('submit') == "view"){
            return view('admin/reports/report',$data);
        }else{
            $dompdf = new \Dompdf\Dompdf(); 
            $dompdf->loadHtml(view('admin/reports/report',$data));
            $dompdf->setPaper('A4', 'landscape');
            $dompdf->render();
            $dompdf->stream();
        }
        
        //return $this->response->redirect(site_url('/admin/time_tracker/'));
    }
}
