<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\ProjectModel;
use App\Models\TimeTrackerModel;
use App\Models\CompanyModel;

class AdminController extends BaseController
{
    public function __construct(){
        if (session()->get('role') != "admin") {
            echo 'Access denied';
            exit;
        }
    }
    public function index()
    {
        $data=[];
        $userModel=new UserModel();
        $projectModel=new ProjectModel();
        $ticketModel=new TimeTrackerModel();
        $companyModel=new CompanyModel();
        

        $data['user']=$userModel->countAll();
        $data['company']=$companyModel->countAll();
        $data['project']=$projectModel->countAll();
        $data['ticket']=$ticketModel->countAll();
        return view("admin/dashboard",$data);
    }
}
