<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use App\Models\CityModel;
use App\Models\StateModel;

class UserController extends BaseController
{
    
    public function login(){
        $data = [];

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'email' => 'required|min_length[6]|max_length[50]|valid_email',
                'password' => 'required|min_length[8]|max_length[255]|validateUser[email,pass]',
            ];

            $errors = [
                'password' => [
                    'validateUser' => "Email or Password didn't match",
                ],
            ];

            if (!$this->validate($rules, $errors)) {

                return view('login', [
                    "validation" => $this->validator,
                ]);

            } else {
                $model = new UserModel();

                $user = $model->where('email', $this->request->getVar('email'))
                    ->first();

                // Stroing session values
                $this->setUserSession($user);

                // Redirecting to dashboard after login
                if($user['role'] == "admin"){

                    return redirect()->to(base_url('admin'));

                }elseif($user['role'] == "employee"){

                    return redirect()->to(base_url('employee'));
                }
            }
        }
        return view('login');
    }

    private function setUserSession($user){
        $data = [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'isLoggedIn' => true,
            "role" => $user['role'],
            "profilepic" => $user['profilepic'],
        ];

        session()->set($data);
        return true;
    }

    public function logout(){
        session()->destroy();
        return redirect()->to('login');
    }

    public function myprofile(){
        $data = [];
        $model = new UserModel();
        $citymodel = new CityModel();
        $stateModel = new StateModel();

        $data['user'] = $model->where('id', session()->get('id'))->first();
        $data['citys'] = $citymodel->findAll();
        $data['states'] = $stateModel->findAll();

         

        if ($this->request->getMethod() == 'post') {
            if(!empty($_FILES['avatar']['name'])){
                $rules = [
                    'name' => 'required',
                    'email' => 'required|min_length[6]|max_length[50]|valid_email',
                    'phone' => 'required',
                    'dob' => 'required|date',
                    'address' => 'required|max_length[255]',
                    'city' => 'required',
                    'avatar' => 'uploaded[avatar]|mime_in[avatar,image/jpg,image/jpeg,image/png]',
                ];

                $errors = [];

                if (!$this->validate($rules, $errors)) {
                    $data['validation'] = $this->validator;
                    return view('myprofile', $data);

                } else {
                    $img = $this->request->getFile('avatar');
                    $img->move(ROOTPATH.'public/profile_image/', $img->getName());
                    
                    $model = new UserModel();
                    $userdata = [
                        'name' =>  $this->request->getVar('name'), 
                        'email' =>  $this->request->getVar('email'),
                        'dob' =>  $this->request->getVar('dob'),
                        'phoneno' =>  $this->request->getVar('phone'),
                        'address' =>  $this->request->getVar('address'),
                        'city_id' =>  $this->request->getVar('city'),
                        'profilepic' =>  $img->getName(),
                    ];
                    $model->update(session()->get('id'),$userdata);
                    $data["success"]="Profile Succefully Updated";
                }
            }else{
                $rules = [
                    'name' => 'required',
                    'email' => 'required|min_length[6]|max_length[50]|valid_email',
                    'phone' => 'required',
                    'dob' => 'required|date',
                    'address' => 'required|max_length[255]',
                    'city' => 'required',
                ];

                $errors = [];

                if (!$this->validate($rules, $errors)) {
                    $data['validation'] = $this->validator;
                    return view('myprofile', $data);

                } else {
                    $model = new UserModel();
                    $userdata = [
                        'name' =>  $this->request->getVar('name'), 
                        'email' =>  $this->request->getVar('email'),
                        'dob' =>  $this->request->getVar('dob'),
                        'phoneno' =>  $this->request->getVar('phone'),
                        'address' =>  $this->request->getVar('address'),
                        'city_id' =>  $this->request->getVar('city'),
                    ];
                    $model->update(session()->get('id'),$userdata);
                    $data["success"]="Profile Succefully Updated";
                }
            }
            
            $session = \Config\Services::session();
            $session->setFlashdata("success", "Profile Updated Sucessfully");

            return $this->response->redirect(site_url('/myprofile'));
            // return view('myprofile', $data);            
        }else{
            
            //echo "<pre>";print_r($data['citys']);exit();
            return view('myprofile',$data);
        }
    }

     public function changepassword(){
        $data = [];

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'oldpassword' => 'required|min_length[8]',
                'newpassword' => 'required|min_length[8]',
                'confirm_password' => 'required|min_length[8]|matches[newpassword]',
            ];

            $errors = [];

            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
                return view('changepassword', $data);
            } else {
                $usermodel=new UserModel();
                $userDetails=$usermodel->where("id",session()->get("id"))->first();
                if (password_verify($this->request->getVar('newpassword'), $userDetails['pass'])){
                    $session = \Config\Services::session();
                    $session->setFlashdata("error", "User password not match");
                    return $this->response->redirect(site_url('/changepassword'));
                }else{
                    $userdata=[
                        'pass' => password_hash($this->request->getVar('newpassword'),PASSWORD_DEFAULT),
                    ];
                    $usermodel->update(session()->get("id"),$userdata);
                    $session = \Config\Services::session();
                    $session->setFlashdata("success", "User password succefully changed");
                    return $this->response->redirect(site_url('/changepassword'));
                }
            }
        }else{
            return view('changepassword');
        }
    }
}
