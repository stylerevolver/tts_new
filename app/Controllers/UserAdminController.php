<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CityModel;
use App\Models\UserModel;

class UserAdminController extends BaseController
{
    public $session;
    public $db;

    public function __construct()
    {
        $this->db = \Config\Database::connect();
        $this->session = \Config\Services::session();
    }

    // show users list
    public function index(){
        
        $sql="SELECT u.*,c.name as city_name FROM `users` as u,city c WHERE u.city_id=c.id";    
        $query = $this->db->query($sql);
        $data['users'] = $query->getResult();
        return view('admin/user/view', $data);
    }

    // add user form
    public function create(){

        $data=[];
        $cityModel = new CityModel();
        $data['city'] = $cityModel->findAll();
        return view('admin/user/create',$data);
    }
 
    // insert data
    public function store() {
        $errors = [];
        
        $model = new UserModel();
        
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|is_unique[users.email]|max_length[50]|valid_email',
            'phoneno' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'dob' => 'required|date',
        ];

        $password=$this->random_password();
        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/user_create/'));
        } else {
            $userData = [
                'name' =>  $this->request->getVar('name'), 
                'email' =>  $this->request->getVar('email'), 
                'pass' =>  password_hash($password, PASSWORD_DEFAULT),
                'phoneno' =>  $this->request->getVar('phoneno'),
                'gender' =>  $this->request->getVar('gender'),
                'address' =>  $this->request->getVar('address'),
                'city_id' =>  $this->request->getVar('city'),
                'dob' =>  $this->request->getVar('dob'),
                'status' =>  1,
                "role" => "employee"
            ];
            $model->insert($userData);

            //mail process
            $to = $this->request->getVar('email');
            $subject = "User Registration";
            $message = "<div style='border:1px solid black;border-radius:10px;padding:20px;background: aliceblue;'>
                        <h1>Hii, ". $this->request->getVar('name')." </h1><hr/><h3>Your authentication details</h3><hr/>
                        <h1>
                            Email Id : ". $this->request->getVar('email')
                        ."</h1>
                        <h1>
                            Password : ". $password
                        ."</h1></div>";
            
            $email = \Config\Services::email();
            $email->setTo($to);
            $email->setFrom('globerain123@gmail.com', 'User Registration');
            
            $email->setSubject($subject);
            $email->setMessage($message);
            try{
                if ($email->send()){
                    $this->session->setFlashdata("success", "User Created Sucessfully");
                    return $this->response->redirect(site_url('/admin/user/'));
                }
            }catch (Exception $e) {
                $this->session->setFlashdata("error", "Email not send somthing is wrong in mail authentication.");
                return $this->response->redirect(site_url('/admin/user_create/'));
            }
        }
        
    }

    // show single user
    public function edit($id = null){
        
        $userModel = new UserModel();
        $cityModel = new CityModel();
        $data['city'] = $cityModel->findAll();
        $data['user'] = $userModel->where('id', $id)->first();
        return view('admin/user/edit', $data);

    }

    // update user data
    public function update(){
        $errors = [];
       $model = new UserModel();
        
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|validateEmail[email,id]|max_length[50]|valid_email',
            'phoneno' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'city' => 'required',
            'dob' => 'required|date',
        ];
        $errors = [
                'email' => [
                    'validateEmail' => "Email must be unique",
                ],
            ];
        $password=$this->random_password();
        if (!$this->validate($rules, $errors)) {
            $this->session->setFlashdata("validation", $this->validator);
            return $this->response->redirect(site_url('/admin/user_edit/'.$this->request->getVar('id')));
        } else {
            $userData = [
                'name' =>  $this->request->getVar('name'), 
                'email' =>  $this->request->getVar('email'), 
                'phoneno' =>  $this->request->getVar('phoneno'),
                'gender' =>  $this->request->getVar('gender'),
                'address' =>  $this->request->getVar('address'),
                'city_id' =>  $this->request->getVar('city'),
                'dob' =>  $this->request->getVar('dob'),
            ];
            $model->update($this->request->getVar('id'),$userData);

            $this->session->setFlashdata("success", "User was updated sucessfully");
            return $this->response->redirect(site_url('/admin/user/'));
        }
    }
 
    // delete user
    public function delete($id = null){
        $userModel = new UserModel();
        $data['user'] = $userModel->where('id', $id)->delete($id);
        $this->session->setFlashdata("success", "User Deleted Sucessfully");
        return $this->response->redirect(site_url('/admin/user/'));
    } 

    public function random_password(){
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$&';
        $password = array(); 
        $alpha_length = strlen($alphabet) - 1; 
        for ($i = 0; $i < 8; $i++) 
        {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password); 
    }
}
