<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\ProjectModel;
use App\Models\TimeTrackerModel;
use App\Models\UserTimeTrackerModel;
use App\Models\CompanyModel;

class EmployeeController extends BaseController
{
    public $db;
    public function __construct()
    {
        if (session()->get('role') != "employee") {
            echo 'Access denied';
            exit;
        } 
        $this->db = \Config\Database::connect();

    }
    public function index()
    {
        $data=[];
        
        $ticketModel=new TimeTrackerModel();
        

        $projectSql="SELECT count(*) as count FROM `project` WHERE id in (select project_id from time_tracker where user_id='".session()->get('id')."');";
        $query = $this->db->query($projectSql);
        $output=$query->getResult();
        $data['project'] = $output[0]->count;

        
        $data['open_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","created")->findAll();
        $data['close_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","done")->findAll();
        $data['on_progress_ticket']=$ticketModel->where('user_id',session()->get('id'))->where("status","in_progress")->findAll();

 
        $data['all_tickets']=$ticketModel->where('user_id',session()->get('id'))->findAll();
        $data['on_progress_tickets']=$ticketModel->where('user_id',session()->get('id'))->where("status","in_progress")->findAll();

        return view("employee/dashboard",$data);
    }
    public function ticketDetails($id = null){
        $data=[];
        
        $ticketModel=new TimeTrackerModel();
        $sql="SELECT t.id, t.name, t.description, t.startdate, t.enddate, t.status, c.name AS company_name, p.name AS project_name, u.name AS user_name FROM `time_tracker` AS t, company c, project p, users u WHERE t.company_id=c.id and t.project_id=p.id and u.id=t.user_id and t.id=$id;";
        $query = $this->db->query($sql);
        $data['ticket'] = $query->getResult();
        
        
        $timeDetatilsSql="SELECT * FROM `user_time_tracker` WHERE `time_tracker_id`=$id and `date`=CURRENT_DATE ORDER BY id DESC LIMIT 1;";

        $tquery = $this->db->query($timeDetatilsSql);
        $data['ticket_details'] = $tquery->getResult();
        
        
        $time="";
        $time_para="00:00:00";
        $time_s=00;
        $time_h=00;
        $time_m=00;
        $created_at=00;
        if(count($data['ticket_details']) > 0){
            $time=$data['ticket_details'][0]->time;
            $time_para=explode(':',$time);
            if ($data['ticket_details'][0]->status == "in_progress" || $data['ticket_details'][0]->status == "resume"){
                $cat = strtotime($data['ticket_details'][0]->created_at);
                $ctime=date('H:i:s',$cat);
                date_default_timezone_set('Asia/Kolkata');
                $now = date("H:i:s");
                $diff= strtotime($now) - strtotime($ctime);
                date_default_timezone_set('UTC');
                $add=$diff+strtotime($time);
                date_default_timezone_set('UTC');
                $final_time=date('H:i:s', $add);

                $time_h=date('H', $add);
                $time_m=date('i', $add);
                $time_s=date('s', $add);
            }else{
                $time_h=$time_para[0];
                $time_m=$time_para[1];
                $time_s=$time_para[2];
            }
        }
        $data['time_s']=$time_s;
        $data['time_h']=$time_h;
        $data['time_m']=$time_m;
        //print_r($data['ticket_details']);exit;
        return view("employee/ticket/view",$data);   
    }

    public function store(){

        $userModel=new UserTimeTrackerModel();
        $timeTrackerModel=new TimeTrackerModel();
        $id=$this->request->getVar('id');
        $status=$this->request->getVar('status');
        $time_status=$this->request->getVar('time_status');
        date_default_timezone_set('Asia/Kolkata');
        $data = [
            'time_tracker_id' => $id, 
            'time' =>  $this->request->getVar('time'),
            'remarks' =>  $this->request->getVar('remark'),
            'status' =>  $this->request->getVar('status'),
            'date' =>  date('Y-m-d'),
        ];

        
        $timeTrackerData = [
            'status' =>  $time_status,
        ];
        
        $timeTrackerModel->update($id,$timeTrackerData);

        if ($userModel->insert($data)) {
            $response = [
                'success' => true,
                'msg' => "Remarks created",
            ];
        } else {
            $response = [
                'success' => true,
                'msg' => "Failed to remarks insert",
            ];
        }
        return $this->response->setJSON($response);
    }
    public function status_update(){

        $timeTrackerModel=new TimeTrackerModel();
        $id=$this->request->getVar('id');
        $status=$this->request->getVar('status');
        
        $data = [
            'status' =>  $this->request->getVar('status'),
        ];

        if ($timeTrackerModel->update($id,$data)) {
            $response = [
                'success' => true,
                'msg' => "Remarks created",
            ];
        } else {
            $response = [
                'success' => true,
                'msg' => "Failed to remarks insert",
            ];
        }
        return $this->response->setJSON($response);
    }
    
    public function openStore(){

        $timeTrackerModel=new TimeTrackerModel();

        $id=$this->request->getVar('id');
        $status=$this->request->getVar('status');
        $data = [
            'status' =>  $status,
        ];

        if ($timeTrackerModel->update($id,$data)) {
            $response = [
                'success' => true,
                'msg' => "Status created",
                'status' => $status 
            ];
        } else {
            $response = [
                'success' => true,
                'msg' => "Failed to remarks insert",
                'status' => ""
            ];
        }

        return $this->response->setJSON($response);
    }
}
