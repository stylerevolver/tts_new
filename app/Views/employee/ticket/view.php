<?= $this->extend("layouts/app") ?>
<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
 
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
        <div class="flex-grow-1 flex-shrink-0 me-5">
            <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                <h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
                    <span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
                    <small class="text-muted fs-7 fw-bold my-1 ms-1">Task</small>
                </h1>
            </div>
        </div>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
        <div class="col-xl-12" style="box-shadow: 0px 0px 10px 10px whitesmoke;">
            <div class="card">
                <div class="card-body">
                    <hr/>
                    <div class="row">
                        <?php if($ticket[0]->status != "done"){?>
                        <div class="col-8">
                        <?php }else{ ?>
                        <div class="col-12">

                        <?php } ?>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Name : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><?php echo $ticket[0]->name;?></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Desctiption : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><?php echo nl2br($ticket[0]->description);?></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Start Date : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><?php echo date("d-m-Y", strtotime($ticket[0]->startdate));?></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">End Date : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><?php echo date("d-m-Y", strtotime($ticket[0]->enddate));?></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Project Name : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><b><?php echo $ticket[0]->project_name;?></b></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Company Name : </span>
                                </div>
                                <div class="col-7">
                                    <span class="head-2"><b><?php echo $ticket[0]->company_name;?></b></span>
                                </div>
                            </div>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Status : </span>
                                </div>
                                <div class="col-7">
                                    <?php 
                                    if ($ticket[0]->status == "created"){ ?>
                                        <h4 class="card-text bg-primary" style="color:black;background: #456780;width: fit-content;padding: 5px;border-radius: 5px;">Status :<?php  echo ("Created"); ?></h4>
                                    <?php  }else if($ticket[0]->status == "in_progress"){ ?>
                                        <h4 class="card-text bg-success" style="color:black;background: #456780;width: fit-content;padding: 5px;border-radius: 5px;">Status :<?php  echo ("On Going"); ?></h4>
                                    <?php  }else{ ?>
                                        <h4 class="card-text bg-info" style="color:black;background: #456780;width: fit-content;padding: 5px;border-radius: 5px;">Status :<?php  echo ("Done"); ?></h4>
                                    <?php  } ?>
                                </div>

                            </div>
                        
                        <?php if($ticket[0]->status != "done"){?>
                            <div class="row task_details">
                                <div class="col-5">
                                    <span class="head-1">Status Action : </span>
                                </div>
                                <div class="col-5">
                                    <select name="status" id="test_status" aria-label="Select a status" data-control="select2" data-placeholder="Select a status" class="form-select form-select-solid form-select-lg fw-bold select2-hidden-accessible" data-select2-id="select2-data-10-mby1" tabindex="-1" aria-hidden="true">
                                        <option value="" data-select2-id="select2-data-10-nj9e" selected="selected">Select a status...</option>
                                        <option value="in_progress" <?php if ($ticket[0]->status == "in_progress") { echo "selected";}?>>In Progress</option>
                                        <option value="done" <?php if ($ticket[0]->status == "done") { echo "selected";}?>>Done</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <input type="hidden" name="status_url" id="status_url" value="<?php echo base_url('employee/ticket/status_update');?>" class="form-control">
                                    <input type="button" name="status_update" id="status_update" value="Status Update" class="btn btn-danger">
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                        <!-- <hr/> -->
                        <?php if($ticket[0]->status != "done"){?>
                            <div class="col-4" style="">
                                <section class="clock">
                                    <div class="container">
                                        <h4>Time Manage</h4>
                                        <div class="row">
                                            <div class="col-md-8 input-wrapper">
                                                <div class="input" style="display: none;">
                                                    <input type="number" id="num" class="form-control" min="0">
                                                </div>
                                                
                                                <div class="buttons-wrapper" style="
                                                    <?php 
                                                    if((count($ticket_details) > 0) &&  ("in_progress" == $ticket_details[0]->status)){
                                                        echo 'display: none';
                                                    } ?> 
                                                ">
                                                    <button class="btn" id="start-cronometer">Start Timer</button>
                                                </div>
                                            </div>
                                            <div id="timer" class="col-12">
                                              <div class="clock-wrapper" style="<?php if((count($ticket_details) == 0)){ echo 'display:none';} ?> ">
                                                  <span class="hours"><?php echo $time_h;?></span>
                                                  <span class="dots">:</span>
                                                  <span class="minutes"><?php echo $time_m;?></span>
                                                  <span class="dots">:</span>
                                                  <span class="seconds"><?php echo $time_s;?></span>
                                              </div>
                                            </div>
                                            <div class="buttons-wrapper col-12" style="<?php if((count($ticket_details) == 0)){ echo 'display:none';} ?> ">
                                                <button class="btn btn-success col-md-4 col-lg-4" id="resume-timer" style="margin-left:2px;">Resume</button>
                                                <button class="btn btn-danger col-md-4 col-lg-4" id="stop-timer" style="margin-left:2px;">Pause</button>
                                                <button class="btn btn-dark col-md-4 col-lg-4" id="reset-timer" style="margin-left:2px;">Stop</button>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        <?php }?>
                    </div>
                    <hr/>
                </div>
            </div>
        </div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2>Time Management</h2>
                <div class="btn btn-sm btn-icon btn-active-color-primary customcancel" data-bs-dismiss="modal">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <form method="post" id="reviewfrom">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">
                            Enter the remarks
                        </label>
                        <input type="hidden" name="url" id="url" value="<?php echo base_url('employee/ticket/store');?>" class="form-control" required>
                        <input type="hidden" name="id" id="id" value="<?php echo $ticket[0]->id;?>" class="form-control" required>
                        <input type="text" maxlength="255" name="remarks" id="remarks" value="" class="form-control" required>
                        <input type="hidden" id="time_status" value="<?php echo $ticket[0]->status;?>">
                        <input type="hidden" id="status" value="<?php if((count($ticket_details) > 0)){ echo $ticket_details[0]->status;} ?>">
                        <span class="err_msg"></span>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-end">
                    <a href="#" class="btn btn-active-light me-5 customcancel" data-bs-dismiss="modal">Cancel</a>
                    <button type="button" id="myModal_button" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>