<!DOCTYPE html>
<html lang="en">
 	<head>
		<?=$this->include("layouts/header")?>
  	</head>
  	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed">
    	<div class="d-flex flex-column flex-root">
      		<div class="page d-flex flex-row flex-column-fluid">
        		<?=$this->include("layouts/leftbar")?>
        		<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
          			<div id="kt_header" style="" class="header align-items-stretch">
            			<div class="container-fluid d-flex align-items-stretch justify-content-between">
              				<!--Mobile toggle button--->
              				<div class="d-flex align-items-center d-lg-none ms-n1 me-2" title="Show aside menu">
                				<div class="btn btn-icon btn-active-color-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
                  					<span class="svg-icon svg-icon-2x mt-1">
                    					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                      						<path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                      						<path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                    					</svg>
                  					</span>
                  				</div>
              				</div>
              				<!--Mobile toggle button--->
              				<div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
				                <a href="<?php echo base_url('/dashboard');?>" class="d-lg-none">
				                	<img alt="Logo" src="<?php echo base_url('assets/media/logos/logo-2.svg');?>" class="h-30px" />
				                </a>
				            </div>
              				<!--end::Mobile logo-->
              				<div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
	                			<!--begin::Navbar-->
				                <div class="d-flex align-items-stretch" id="kt_header_nav">
				                  	<!--begin::Menu wrapper-->
					                <div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
					                    <!--begin::Menu-->
					                   	<div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true">
						                    <div  class="menu-item here show menu-lg-down-accordion me-lg-1">
						                        <a class="menu-link active py-3" href="<?php echo base_url("/");?>">
						                          	<span class="menu-title">Dashboards</span>
						                          	<span class="menu-arrow d-lg-none"></span>
						                        </a>
						                    </div>
					                    </div>
					                </div>
				                </div>
	                			<!--end::Navbar-->
				                <!--begin::Topbar-->
				                <div class="d-flex align-items-stretch flex-shrink-0">
				                  	<div class="d-flex align-items-stretch flex-shrink-0">
				                   		<div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
				                      		<div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
				                        		<img src="<?=base_url().'/profile_image/'.session()->get('profilepic')?>" onerror="if (this.src != 'error.jpg') this.src = '<?php echo base_url('assets/media/avatars/150-26.jpg');?>';" alt="image" />
				                      		</div>
				                      		<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
				                        		<div class="menu-item px-3">
				                          			<div class="menu-content d-flex align-items-center px-3">
				                            		<!--begin::Avatar-->
				                            			<div class="symbol symbol-50px me-5">
				                              				<img alt="Logo" src="<?php echo base_url('assets/media/avatars/150-26.jpg');?>" />
				                            			</div>
					                            		<div class="d-flex flex-column">
					                              			<div class="fw-bolder d-flex align-items-center fs-5"><?= session()->get('name') ?> 
					                              				
					                              			</div>
					                              			<a href="#" class="fw-bold text-muted text-hover-primary fs-7"><?= session()->get('email') ?></a>
					                            		</div>
				                            		</div>
				                        		</div>
				                        		<div class="separator my-2"></div>
				                        		<div class="menu-item px-5">
				                          			<a href="<?php echo base_url('myprofile')?>" class="menu-link px-5">My Profile</a>
				                        		</div>
				                        		<div class="menu-item px-5">
				                          			<a href="<?php echo base_url('changepassword')?>" class="menu-link px-5">Change Password</a>
				                        		</div>
				                        		<div class="menu-item px-5">
				                          			<a href="<?php echo base_url('logout');?>" class="menu-link px-5">Sign Out</a>
				                        		</div>
				                        		<div class="separator my-2"></div>
				                        	</div>
				                      	</div>
				                  	</div>
				                </div>
	                			<!--end::Topbar-->
              				</div>
              				<!--end::Wrapper-->
            			</div>
            		</div>
	            	<?= $this->renderSection("body") ?>  
			        
			        <!--begin::Footer-->
			        <div class="footer py-4 d-flex flex-lg-column" id="kt_footer" style="text-align: center;">
			            <!--begin::Container-->
			            <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
			              	<div class="text-dark order-2 order-md-1">
				                <span class="text-muted fw-bold me-1">2021©</span>
			                	<a href="<?php echo base_url("/");?>" class="text-gray-800 text-hover-primary">TTS</a>
			              	</div>
			            </div>
			        </div>
			        <!--end::Footer-->
       	 		</div>
       		</div>
    	</div>

    	<!----Scroll Bar--->
    	<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
      		<span class="svg-icon">
		        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
		          	<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
		          	<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
		        </svg>
		   	</span>
    	</div>
    	<!----Scroll Bar--->
    	
    	<?=$this->include("layouts/footer")?>
  	
  	</body>
</html>