<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">Ticket List</small>
          		</h1>
          	</div>
        </div>
    </div>
</div>



<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
	<div id="kt_content_container" class="container-xxl" style="margin-bottom: 10px;padding: 10px;">
    	<div class="card">
    		<div class="card-body pt-0">
    			<div class="row">
					<div class="col-3"><h3>Project Name:</h3> <?php echo $tickets[0]->name;?></div>
					<div class="col-3"><h3>Company Name:</h3> <?php echo $tickets[0]->company_name;?></div>
					<div class="col-2"><h3>Start Date:</h3> <?php echo $tickets[0]->startdate;?></div>
					<div class="col-2"><h3>End Date:</h3> <?php echo $tickets[0]->enddate;?></div>
					<div class="col-2"><h3>Status:</h3> <?php echo $tickets[0]->status;?></div>
				</div>
        	</div>
    	</div>
    </div>

  	<div id="kt_content_container" class="container-xxl">
    	<div class="card">
    		<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (isset($validation)) : ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= $validation->listErrors() ?>
		                </div>
		               </div>
		        
	        <?php endif; ?>
					
	        
			<div class="card-body pt-0">
        		<div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

		          <div class="table-responsive">
		            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer table-responsive" id="Company_table" role="grid">
			            <thead>
			                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0" role="row">
			                  <th class="min-w-125px">Id</th>
			                  <th class="min-w-125px">User Name</th>
			                  <th class="min-w-125px">Time</th>
			                  <th class="min-w-125px">Remaks</th>
			                  <th class="min-w-125px">Date</th>
			                  <th class="min-w-125px">Status</th>
			                </tr>
			            </thead>
		              	<tbody class="fw-bold text-gray-600">
		              		<?php
		              			foreach ($tickets_details as $key => $ticket) { 
		              				?>
		              			 	<tr class="odd">
					                  	<td><?php echo $ticket->id;?></td>
					                  	<td><?php echo $ticket->user_name;?></td>
					                  	<td><?php echo $ticket->time;?></td>
					                  	<td><?php if ($ticket->remarks == "") {echo "-";}else{echo $ticket->remarks;};?></td>
					                  	<td><?php echo $ticket->date;?></td>
					                  	<td><?php echo $ticket->status;?></td>
					                </tr>
		              		<?php } ?>
		                </tbody>
		            </table>
		          </div>
		        </div>
		    </div>
    	</div>
  	</div>
</div>

<?= $this->endSection() ?>