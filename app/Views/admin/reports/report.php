<!DOCTYPE html>
<html>
<head>
    <title>Time Tracker</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="content-type" content="text-html; charset=utf-8">
    <style type="text/css">
        html, body, div, span, applet, object, iframe,
        h1, h2, h3, h4, h5, h6, p, blockquote, pre,
        a, abbr, acronym, address, big, cite, code,
        del, dfn, em, img, ins, kbd, q, s, samp,
        small, strike, strong, sub, sup, tt, var,
        b, u, i, center,
        dl, dt, dd, ol, ul, li,
        fieldset, form, label, legend,
        table, caption, tbody, tfoot, thead, tr, th, td,
        article, aside, canvas, details, embed,
        figure, figcaption, footer, header, hgroup,
        menu, nav, output, ruby, main, summary,
        time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font: inherit;
            font-size: 100%;
            vertical-align: baseline;
        }

        html {
            line-height: 1;
        }

        ol, ul {
            list-style: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle;
        }

        q, blockquote {
            quotes: none;
        }
        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none;
        }

        a img {
            border: none;
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, main, summary {
            display: block;
        }

        body {
            font-family: 'Source Sans Pro', sans-serif;
            font-weight: 300;
            font-size: 12px;
            margin: 0;
            padding: 0;
        }
        body a {
            text-decoration: none;
            color: inherit;
        }
        body a:hover {
            color: inherit;
            opacity: 0.7;
        }
        body .container {
            min-width: 500px;
            margin: 0 auto;
            padding: 0 20px;
        }
        body .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }
        body .left {
            float: left;
        }
        body .right {
            float: right;
        }
        body .helper {
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        body .no-break {
            page-break-inside: avoid;
        }

        header {
            margin-top: 20px;
            margin-bottom: 50px;
        }
        header figure {
            float: left;
            width: 60px;
            height: auto;
            margin-right: 10px;
            background-color: #6bacea;
            border-radius: 50%;
            text-align: center;
        }
        header figure img {
            margin-top: 13px;
        }
        header .company-address {
            float: left;
            max-width: 150px;
            line-height: 1.7em;
        }
        header .company-address .title {
            color: #6bacea;
            font-weight: 400;
            font-size: 1.5em;
            text-transform: uppercase;
        }
        header .company-contact {
            float: right;
            height: 60px;
            padding: 0 10px;
            background-color: #6bacea;
            color: white;
        }
        header .company-contact span {
            display: inline-block;
            vertical-align: middle;
        }
        header .company-contact .circle {
            width: 20px;
            height: 20px;
            background-color: white;
            border-radius: 50%;
            text-align: center;
        }
        header .company-contact .circle img {
            vertical-align: middle;
        }
        header .company-contact .phone {
            height: 100%;
            margin-right: 20px;
        }
        header .company-contact .email {
            height: 100%;
            min-width: 100px;
            text-align: right;
        }

        /*main .details {
            margin-bottom: 55px;
        }
        main .details .client {
            width: 50%;
            line-height: 20px;
        }*/
        main .details .client .name {
            color: #6bacea;
        }
        main .details .data {
            width: 50%;
            text-align: right;
        }
        main .details .title {
            margin-bottom: 15px;
            color: #6bacea;
            font-size: 1.5em;
            font-weight: 400;
            text-transform: uppercase;
        }
        main table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            font-size: 0.9166em;
        }
        main table .qty, main table .unit, main table .total {
            width: 15%;
        }
        main table .desc {
            width: 55%;
        }
        main table thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }
        main table thead th {
            padding: 5px 10px;
            background: #6bacea;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #FFFFFF;
            text-align: right;
            color: white;
            font-weight: 400;
            text-transform: uppercase;
        }
        main table thead th:last-child {
            border-right: none;
        }
        main table thead .desc {
            text-align: left;
        }
        main table thead .qty {
            text-align: center;
        }
        main table tbody td {
            padding: 10px;
            background: #6bacea;
            color: black;
            text-align: right;
            border-bottom: 5px solid #FFFFFF;
            border-right: 4px solid #E8F3DB;
        }
        main table tbody td:last-child {
            border-right: none;
        }
        main table tbody h3 {
            margin-bottom: 5px;
            color: #6bacea;
            font-weight: 600;
        }
        main table tbody .desc {
            text-align: left;
        }
        main table tbody .qty {
            text-align: center;
        }
        main table.grand-total {
            margin-bottom: 45px;
        }
        main table.grand-total td {
            padding: 5px 10px;
            border: none;
            color: #777777;
            text-align: right;
        }
        main table.grand-total .desc {
            background-color: transparent;
        }
        main table.grand-total tr:last-child td {
            font-weight: 600;
            color: #6bacea;
            font-size: 1.18181818181818em;
        }

        footer {
            margin-bottom: 20px;
        }
        footer .thanks {
            margin-bottom: 40px;
            color: #6bacea;
            font-size: 1.16666666666667em;
            font-weight: 600;
        }
        footer .notice {
            margin-bottom: 25px;
        }
        footer .end {
            padding-top: 5px;
            border-top: 2px solid #6bacea;
            text-align: center;
        }
    </style>
</head>

<body>
    <header class="clearfix" style="height:50px">
        <div class="container" style="margin-bottom:150px;width: 100%;display: flex;">
            <div style="width: 97%;text-align:right;">
                <h2 class="name" style="background-color:#6bacea;color:white;line-height: 26px;font-weight: 700px;font-size:26px">TTS</h2>
                <p style="margin-top:5px">Ahmedabad, Gujarat 380058, IN</p>
                <a href="tel:877 536 3789" style="margin-top:5px">+91 877 536 3789</a>
                <a href="mailto:webdesk@webdesk.in" style="margin-top:3px">webdesk@webdesk.in</a>
                <p style="margin-top:5px"><?php echo $now;?></p>
            </div>
        </div>
    </header>

    <main style="">
        <div class="container" style="margin-top:7px;">
            <table id="area_datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                    <tr>
                        <th colspan="7"><h1 class="name" style="background-color:#6bacea;font-size: 20px;color:white;text-align: center;"><?php echo $title;?></h1></th>
                    </tr>
                  <tr style="text-align:center;">
                    <th style="text-align:center;font-weight: 700;">Id</th>
                    <th style="text-align:center;font-weight: 700;">User Name</th>
                    <th style="text-align:center;font-weight: 700;">Project Name</th>
                    <th style="text-align:center;font-weight: 700;">Time</th>
                    <th style="text-align:center;font-weight: 700;">Remaks</th>
                    <th style="text-align:center;font-weight: 700;">Date</th>
                    <th style="text-align:center;font-weight: 700;">Time Tracker Status</th>
                  </tr>
                </thead>
                <tbody>
                <?php 
                    if (count($tickets_details) > 0){
                      foreach($tickets_details as $key=> $data){ ?>
                      <tr>
                        <td style="text-align:center;"><?php echo $data->id;?></td>
                        <td style="text-align:center;"><?php echo $data->user_name;?></td>
                        <td style="text-align:center;"><?php echo $data->project_name;?></td>
                        <td style="text-align:center;"><?php echo $data->time;?></td>
                        <td style="text-align:center;"><?php echo $data->remarks;?></td>
                        <td style="text-align:center;"><?php echo date("d-m-Y", strtotime($data->date));?></td>
                        <td style="text-align:center;"><?php echo $data->status;?></td>
                    <?php } }else{ ?>

                        <th colspan="7"><h1 class="name" style="background-color:#6bacea;font-size: 22px;color:white;text-align: center;">No Record found</h1></th>

                    <?php } ?>
                </tbody>
            </table>
        </div>
    </main>
</body>
</html>
