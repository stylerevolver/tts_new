<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">Company List</small>
          		</h1>
          		
          		
        	</div>

        </div>

    </div>
</div>

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">

  	<div id="kt_content_container" class="container-xxl">
    	<div class="card">
    		<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (isset($validation)) : ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= $validation->listErrors() ?>
		                </div>
		               </div>
		        
	        <?php endif; ?>
					
		
      		<!-- <div class="card-header border-0 pt-6">
        		<div class="card-title">
          			<div class="d-flex align-items-center position-relative my-1">
            			<span class="svg-icon svg-icon-1 position-absolute ms-6">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
								<rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
								<path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black"></path>
							</svg>
            			</span>
            			<input type="text" data-kt-customer-table-filter="search" class="form-control form-control-solid w-250px ps-15" placeholder="Search Customers">
          			</div>
          		</div>
		        <div class="card-toolbar ">
		          	<div class="d-flex justify-content-end" data-kt-customer-table-toolbar="base">
			            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_add_customer">Add Company</button>
		          	</div>
		        </div>
		    </div> -->
      		<div class="card-body pt-0">
        		<div id="kt_customers_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
		          <div class="table-responsive">
		            <table class="table align-middle table-row-dashed fs-6 gy-5 dataTable no-footer" id="Company_table" role="grid">
			            <thead>
			                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0" role="row">
			                  <th class="min-w-125px sorting sorting_desc" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Customer Name: activate to sort column ascending" style="width: 164.775px;" aria-sort="descending">Company Name</th>
			                  <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 181.3px;">Email Id</th>
			                  <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Company: activate to sort column ascending" style="width: 164.775px;">Phone Number</th>
			                  <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Payment Method: activate to sort column ascending" style="width: 164.775px;">Address</th>
			                  <th class="min-w-125px sorting" tabindex="0" aria-controls="kt_customers_table" rowspan="1" colspan="1" aria-label="Created Date: activate to sort column ascending" style="width: 124.775px;">City</th>
			                  <th class="min-w-125px sorting_disabled" rowspan="1" colspan="1" aria-label="Actions" style="width: 116.85px;">Actions</th>
			                </tr>
			            </thead>
		              	<tbody class="fw-bold text-gray-600">
		              		<?php
		              			foreach ($companys as $key => $company) { 
		              				?>
		              			 	<tr class="odd">
					                  	<td class="sorting_1"><?php echo $company->name;?></td>
					                  	<td><?php echo $company->email;?></td>
					                  	<td><?php echo $company->phoneno;?></td>
					                  	<td><?php echo $company->address;?></td>
					                  	<td><?php echo $company->city_name;?></td>
					                  	<td class="text-center">
											<a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
												<span class="svg-icon svg-icon-5 m-0">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
													</svg>
												</span>
											</a>
											<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
												<div class="menu-item px-3">
													<a href="<?php echo base_url('admin/company_edit/'.$company->id);?>" class="menu-link px-3">Edit</a>
												</div>
												<div class="menu-item px-3">
													<a href="<?php echo base_url('admin/company_delete/'.$company->id);?>" class="menu-link px-3" data-kt-customer-table-filter="delete_row" onclick="return confirm('Are you sure do you want to delete this record?');">Delete</a>
												</div>
											</div>
										</td>
					                </tr>
		              		<?php } ?>
		                </tbody>
		            </table>
		          </div>
		        </div>
		    </div>
    	</div>
  	</div>
</div>

<?= $this->endSection() ?>