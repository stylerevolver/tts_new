<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">My Profile</small>
          		</h1>
        	</div>
        </div>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
		<div class="card mb-5 mb-xl-10">
			<!--begin::Card header-->
			<?php 
				if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('success');?>
						</div>
					</div>
				<?php }
				if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
					<div class="col-12" style="margin: 11px;width: 98%;">
						<div class="alert alert-success" role="alert">
						  <?php echo session()->getFlashdata('error');?>
						</div>
					</div>
				<?php }
			 	if (isset($validation)) : ?>
			 	    <div class="col-12" style="margin: 11px;width: 98%;">
		                <div class="alert alert-danger" role="alert">
		                    <?= $validation->listErrors() ?>
		                </div>
		               </div>
		        
	        <?php endif; ?>
			<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
				<div class="card-title m-0">
					<h3 class="fw-bolder m-0">Profile Details</h3>
				</div>
			</div>
			
			
			<div id="kt_account_profile_details" class="collapse show">
				<!--begin::Form-->
				<form action="<?= base_url('myprofile') ?>" method="post" class="form fv-plugins-bootstrap5 fv-plugins-framework" enctype="multipart/form-data">
					<!--begin::Card body-->
					<div class="card-body border-top p-9">
						<!--begin::Input group-->
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
							<div class="col-lg-8">
								<div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(assets/media/avatars/blank.png)">
									<div class="image-input-wrapper w-125px h-125px" style="background-image: url('<?=base_url().'/profile_image/'.$user["profilepic"]?>')"></div>
									<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="" data-bs-original-title="Change avatar"> <i class="bi bi-pencil-fill fs-7"></i>
										<input type="file" name="avatar" accept=".png, .jpg, .jpeg">
										<input type="hidden" name="avatar_remove">
									</label>
									<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="" data-bs-original-title="Cancel avatar">
										<i class="bi bi-x fs-2"></i>
									</span>
									<!-- <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="" data-bs-original-title="Remove avatar">
										<i class="bi bi-x fs-2"></i>
									</span> -->
								</div>
								<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
							</div>
						</div>
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Full Name</label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="text" name="name" class="form-control form-control-lg form-control-solid" placeholder="name" value="<?php echo $user["name"];?>">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
						</div>
						<div class="row mb-6">
							<!--begin::Label-->
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="email" name="email" class="form-control form-control-lg form-control-solid" placeholder="Email ID" value="<?php echo $user['email'];?>">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
							<!--end::Col-->
						</div>
						<!--end::Input group-->
						<!--begin::Input group-->
						<div class="row mb-6">
							<!--begin::Label-->
							<label class="col-lg-4 col-form-label  fw-bold fs-6"> <span class="required">Contact Phone</span> <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Phone number must be active" aria-label="Phone number must be active"></i> </label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="tel" name="phone" class="form-control form-control-lg form-control-solid" placeholder="Phone number" value="<?php echo $user['phoneno'];?>">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
							<!--end::Col-->
						</div>
						<!--end::Input group-->
						<!--begin::Input group-->
						<div class="row mb-6">
							<!--begin::Label-->
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Date Of Birth</label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8 fv-row">
								<input type="date" name="dob" class="form-control form-control-lg form-control-solid" placeholder="Company website"value="<?php echo $user['dob'];?>"> </div>
							<!--end::Col-->
						</div>
						<div class="row mb-6">
							<!--begin::Label-->
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Address</label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8 fv-row">
								<textarea name="address" class="form-control form-control-lg form-control-solid" placeholder="Enter Address"><?php echo $user['address'];?></textarea>
							</div>
							<!--end::Col-->
						</div>
						<!--end::Input group-->
						<!--begin::Input group-->
						<div class="row mb-6">
							<!--begin::Label-->
							<label class="col-lg-4 col-form-label fw-bold fs-6"> <span class="required">City</span> <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="City" aria-label="City"></i> </label>
							<!--end::Label-->
							<!--begin::Col-->
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<select name="city" aria-label="Select a City" data-control="select2" data-placeholder="Select a City" class="form-select form-select-solid form-select-lg fw-bold select2-hidden-accessible" data-select2-id="select2-data-7-mby1" tabindex="-1" aria-hidden="true">
									<option value="" data-select2-id="select2-data-9-nj9e" selected="selected">Select a City...</option>
									<?php 
										foreach ($citys as $key => $city) { ?>
											<option value="<?php echo $city['id'];?>"
												<?php 
													if ($city['id'] == $user['city_id']){ ?>
														selected="selected"
												<?php 	}
												?>
												><?php echo $city['name'];?></option>
										<?php }
									?>
								</select>
							</div>
							<!--end::Col-->
						</div>
					</div>
					<!--end::Card body-->
					<!--begin::Actions-->
					<div class="card-footer d-flex justify-content-end py-6 px-9">
						<button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
						<button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
					</div>
					<!--end::Actions-->
					<input type="hidden">
					<div></div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Content-->
		</div>
	</div>
</div>
<?= $this->endSection() ?>