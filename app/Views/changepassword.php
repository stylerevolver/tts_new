<?= $this->extend("layouts/app") ?>

<?= $this->section("body") ?>
<div class="toolbar py-2" id="kt_toolbar">
    <div id="kt_toolbar_container" class="container-fluid d-flex align-items-center">
      	<div class="flex-grow-1 flex-shrink-0 me-5">
        	<div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
          		<h1 class="d-flex align-items-center text-dark fw-bolder my-1 fs-3">Dashboard
            		<span class="h-20px border-gray-200 border-start ms-3 mx-2"></span>
            		<small class="text-muted fs-7 fw-bold my-1 ms-1">Change Password</small>
          		</h1>
        	</div>
        </div>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
		<div class="card mb-5 mb-xl-10">
			<?php 
			if (session()->getFlashdata('success') && session()->getFlashdata('success') != ""){?>
				<div class="col-12" style="margin: 11px;width: 98%;">
					<div class="alert alert-success" role="alert">
					  <?php echo session()->getFlashdata('success');?>
					</div>
				</div>
			<?php }
			if (session()->getFlashdata('error') && session()->getFlashdata('error') != ""){?>
				<div class="col-12" style="margin: 11px;width: 98%;">
					<div class="alert alert-danger" role="alert">
					  <?php echo session()->getFlashdata('error');?>
					</div>
				</div>
			<?php }
		 	if (isset($validation)) : ?>
		 	    <div class="col-12" style="margin: 11px;width: 98%;">
	                <div class="alert alert-danger" role="alert">
	                    <?= $validation->listErrors() ?>
	                </div>
	            </div>
	    	<?php endif; ?>
			<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
				<div class="card-title m-0">
					<h3 class="fw-bolder m-0">Change Password</h3> </div>
			</div>
			<div id="kt_account_profile_details" class="collapse show">
				<form action="<?= base_url('changepassword') ?>" method="post" class="form fv-plugins-bootstrap5 fv-plugins-framework">
					<div class="card-body border-top p-9">
						
						<div class="row mb-6">
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Old Password</label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="password" name="oldpassword" class="form-control form-control-lg form-control-solid" placeholder="Old Password" value="">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
						</div>

						<div class="row mb-6">
							<label class="col-lg-4 col-form-label required fw-bold fs-6">New Password</label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="password" name="newpassword" class="form-control form-control-lg form-control-solid" placeholder="New Password" value="">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
						</div>

						<div class="row mb-6">
							<label class="col-lg-4 col-form-label required fw-bold fs-6">Confirm Password</label>
							<div class="col-lg-8 fv-row fv-plugins-icon-container">
								<input type="password" name="confirm_password" class="form-control form-control-lg form-control-solid" placeholder="Confirm Password" value="">
								<div class="fv-plugins-message-container invalid-feedback"></div>
							</div>
						</div>
					</div>
					<div class="card-footer d-flex justify-content-end py-6 px-9">
						<button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
						<button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">Save Changes</button>
					</div>
					<input type="hidden">
					<div></div>
				</form>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>