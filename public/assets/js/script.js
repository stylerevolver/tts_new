function openMOdel(){
    $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
}   

$(document).ready(function(){
    //only close the model when you click on the model close button 
    $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    })

    const measure = $('select#measure')
    const ammount = $('input#num')
    const timer = $('#timer')
    const s = $(timer).find('.seconds')
    const m = $(timer).find('.minutes')
    const h = $(timer).find('.hours')
    
    var seconds = 0
    var minutes = 0
    var hours = 0

    var interval = null;
    var clockType = undefined;

    
    if ($("#status").val().trim() === "in_progress" || $("#status").val().trim() === "resume"){
        $(".clock-wrapper").show();
        $(".buttons-wrapper").show();

        $('button#start-cronometer').hide()
        $('button#resume-timer').hide() //resume
        $('button#stop-timer').show()   //pause
        $('button#reset-timer').show()  //stop

        jQuery(function(){
            $("button#resume-timer").click();
        });
    }else if ($("#status").val().trim() === "pause"){
        $(".clock-wrapper").show();
        $(".buttons-wrapper").show();
        $('button#start-cronometer').hide()
        $('button#resume-timer').show() //resume
        $('button#stop-timer').hide()   //pause
        $('button#reset-timer').show()  //stop
        
    }else if ($("#status").val().trim() === "stop"){
        $(".hours").html("00");
        $(".minutes").html("00");
        $(".seconds").html("00");
        $('button#start-cronometer').show()
        $('#timer').hide()
        $('button#stop-timer').hide()
        $('button#resume-timer').hide()
        $('button#reset-timer').hide()
    }
   
    $(".customcancel").on('click',function(){
        cronometer()
    })

    //start button
    $('button#start-cronometer').on('click', function(){
        clockType = 'cronometer'
        var customtime=$(".hours").html()+":"+$(".minutes").html()+":"+$(".seconds").html();
        if ($(ammount).val() > -1) {
            $.ajax({
                url: $("#url").val(),
                type: "POST",
                cache: false,
                data: {
                    id:$("#id").val(),
                    time_status: "in_progress",
                    status: "in_progress",
                    time:customtime,
                    remark:"start"
                },
                success: function(data) {
                    if (data.success == true) {
                        $(".clock-wrapper").show();
                        $(".buttons-wrapper").show();
                        $('button#start-cronometer').hide()
                        $('button#resume-timer').hide() //resume
                        $('button#stop-timer').show()   //pause
                        $('button#reset-timer').show()  //stop
                        startClock()
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire('Error at add data', '', 'error')
                }
            });
        }
    })

    //model click action
    $('button#myModal_button').on('click', function() {
        if($("#remarks").val() == ""){
            $("#remarks").css("border","1px solid red");
            $(".err_msg").val("Please enter the remarks")
            $('#myModal').modal('show');
            return false
        }else{
            var customtime=$(".hours").html()+":"+$(".minutes").html()+":"+$(".seconds").html();
            $.ajax({
                url: $("#url").val(),
                type: "POST",
                cache: false,
                data: {
                    id:$("#id").val(),
                    time:customtime,
                    remark:$("#remarks").val(),
                    time_status: $("#time_status").val(),
                    status: $("#status").val(),
                },
                success: function(data) {
                    if (data.success == true) {
                        $(".err_msg").val("")
                        $('#myModal').modal('hide');
                        $("#remarks").val("");
                        Swal.fire('Remarks was successfully updated..!', '', 'success')
                        if ($("#status").val() == "stop"){
                            restartClock()
                            $('button#start-cronometer').show()
                        }else if ($("#status").val() == "pause"){
                            $(".clock-wrapper").show();
                            $(".buttons-wrapper").show();
                            $('button#start-cronometer').hide()
                            $('button#resume-timer').show() //resume
                            $('button#stop-timer').hide()   //pause
                            $('button#reset-timer').show()  //stop
                        }
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire('Error at add data', '', 'error')
                }
            });
        }
    })

    $('#status_update').on('click', function() {
       $.ajax({
            url: $("#status_url").val(),
            type: "POST",
            cache: false,
            data: {
                id:$("#id").val(),
                status: $("#test_status").val(),
            },
            success: function(data) {
                if (data.success == true) {
                    Swal.fire('Ticket status was successfully updated..!', '', 'success')
                    setTimeout( function(){ 
                        location.reload();    
                    }  , 5000 );
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire('Error at add data', '', 'error')
            }
        });
    })

    $('button#stop-timer').on('click', function() {
        pauseClock()
    })

    $('button#reset-timer').on('click', function() {
        $("#status").val("stop")
        $('#myModal').modal('toggle');
        clear(interval)
    })

    $('button#resume-timer').on('click', function() {
      $("#status").val("resume")
        clockType = 'cronometer'
        var customtime=$(".hours").html()+":"+$(".minutes").html()+":"+$(".seconds").html();
        if ($(ammount).val() > -1) {
            $.ajax({
                url: $("#url").val(),
                type: "POST",
                cache: false,
                data: {
                    id:$("#id").val(),
                    time_status: "in_progress",
                    status: "resume",
                    time:customtime,
                    remark:"resume"
                },
                success: function(data) {
                    if (data.success == true) {
                        $(".clock-wrapper").show();
                        $(".buttons-wrapper").show();
                        $('button#resume-timer').hide();
                        $('button#reset-timer').show();
                        $('button#stop-timer').show();
                        $('button#start-cronometer').hide();
                        $("#remarks").val("");
                        cronometer()
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    Swal.fire('Error at add data', '', 'error')
                }
            });
        }
    })

    function pad(d) {
        if (d.toString().length == 1){
            return (d < 10) ? '0' + d.toString() : d.toString()
        }else{
            return d.toString()
        }
    }

    function startClock() {
        hasStarted = false
        hasEnded = false

        seconds = 0
        minutes = 0
        hours = 0

        if (seconds <= 10 && clockType == 'countdown' && minutes == 0 && hours == 0) {
          $(timer).find('span').addClass('red')
        }

        refreshClock()

        $('.input-wrapper').slideUp(350)
        setTimeout(function(){
            $('#timer').show()
            $('#stop-timer').show()
        }, 350)
        
        cronometer()
    
    }

    function restartClock() {
        clear(interval)
        hasStarted = false
        hasEnded = false

        seconds = 0
        minutes = 0
        hours = 0

        $(s).text('00')
        $(m).text('00')
        $(h).text('00')

        $(timer).find('span').removeClass('red')

        $('#timer').hide()
        $('#stop-timer').hide()
        $('button#resume-timer').hide()
        $('button#reset-timer').hide()
        setTimeout(function(){
           $('.input-wrapper').slideDown(350)
        },350)
    }

    function pauseClock() {
        clear(interval)
        $("#time_status").val("in_progress");
        $("#status").val("pause");
        $('#myModal').modal('toggle');
    }

    var hasStarted = false
    var hasEnded = false
    if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
        hasEnded = true
    }

    function countdown() {
        hasStarted = true
        interval = setInterval(() => {
            if(hasEnded == false) {
                if (seconds <= 11 && minutes == 0 && hours == 0) {
                  $(timer).find('span').addClass('red')
                }

                if(seconds == 0 && minutes == 0 || (hours > 0  && minutes == 0 && seconds == 0)) {
                    hours--
                    minutes = 59
                    seconds = 60
                    refreshClock()
                }

                if(seconds > 0) {
                    seconds--
                    refreshClock()
                }
                else if (seconds == 0) {
                    minutes--
                    seconds = 59
                    refreshClock()
                }
            }
            else {
                restartClock()
            }

        }, 1000)
    }

    function cronometer() {
        hasStarted = true

        if ($(".seconds").html() > 0){
            seconds=$(".seconds").html();
        }
        if ($(".minutes").html() > 0){
            minutes=$(".minutes").html();
        }
        if ($(".hours").html() > 0){
            hours=$(".hours").html();
        }

        interval = setInterval(() => {
            if (seconds < 59) {
                seconds++
                refreshClock()
            }
            else if (seconds == 59) {
                minutes++
                seconds = 0
                refreshClock()
            }

            if (minutes == 60) {
                hours++
                minutes = 0
                seconds = 0
                refreshClock()
            }

        }, 1000)
    }

    function refreshClock() {
        $(s).text(pad(seconds))
        $(m).text(pad(minutes))
        if (hours < 0) {
            $(s).text('00')
            $(m).text('00')
            $(h).text('00')
        } else {
            $(h).text(pad(hours))
        }

        if (hours == 0 && minutes == 0 && seconds == 0 && hasStarted == true) {
            hasEnded = true
            alert('The Timer has Ended !')
        }
    }

    function clear(intervalID) {
        clearInterval(intervalID)
        console.log('cleared the interval called ' + intervalID)
    }
})